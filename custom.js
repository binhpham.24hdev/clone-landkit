const $ = document.querySelector.bind(document);
const $$ = document.querySelectorAll.bind(document);


// >>>>>>>>>>>> Toggle menu <<<<<<<<<<<<<


const openMenu = $(".header-menu-icon");
const menu = $(".header-menu");
const closeMenu = $(".close-menu i");

const toggleMenu = () => menu.classList.toggle("hide");

openMenu.addEventListener("click", toggleMenu);
closeMenu.addEventListener("click", toggleMenu);

document.addEventListener("keydown", (e) => {
  if (e.which == 27) menu.classList.add("hide");
});


// >>>>>>>>>>>> Menu Account responsive <<<<<<<<<<<<<


const accountMenus = $$(".acc-menu-responsive");
const accSubItems = $$(".acc-sub-responsive");
const arrowIcons = $$(".fa-angle-down");

accountMenus.forEach((item, index) => {
  item.addEventListener("click", () => {
    accSubItems[index].classList.toggle("hide");
    arrowIcons[index].classList.toggle("rotate");
  });
});


// >>>>>>>>>>>> Slider <<<<<<<<<<<<<


const nextMb = $(".arrow-right-mb");
const prevMb = $(".arrow-left-mb");
const nextDt = $(".arrow-right-dt");
const prevDt = $(".arrow-left-dt");
const imgInsta = $(".img-insta");
const imgAirbnb = $(".img-airbnb");
const contentInsta = $(".content-insta");
const contentAirbnb = $(".content-airbnb");

const changeImg = () => {
  imgInsta.classList.toggle("slide-hide-img");
  imgAirbnb.classList.toggle("slide-hide-img");
  imgInsta.classList.toggle("slide-show-img");
  imgAirbnb.classList.toggle("slide-show-img");
};

const action = (els) => {
  els.forEach((el) => el.addEventListener("click", changeImg));
};
action([nextDt, prevDt, nextMb, prevMb]);

let content = "insta";

const slideContent = (direction) => {
  console.log(content);
  if (content == "insta") {
    contentInsta.classList.add(`hide-to-${direction}`);
    contentAirbnb.classList.add(`show-to-${direction}`);
    contentInsta.classList.remove(`show-to-${direction}`);
    contentAirbnb.classList.remove(`hide-to-${direction}`);
    content = "airbnb";
  } else {
    contentInsta.classList.remove(`hide-to-${direction}`);
    contentAirbnb.classList.remove(`show-to-${direction}`);
    contentInsta.classList.add(`show-to-${direction}`);
    contentAirbnb.classList.add(`hide-to-${direction}`);
    content = "insta";
  }
};

nextDt.addEventListener("click", () => slideContent("left"));
prevDt.addEventListener("click", () => slideContent("right"));
nextMb.addEventListener("click", () => slideContent("left"));
prevMb.addEventListener("click", () => slideContent("right"));


// >>>>>>>>>>>> Section price <<<<<<<<<<<<<


const switchTerm = $(".switch");
const price = $(".stand-price h3");

const changePrice = () => {
  let counter = price.innerHTML;

  if (counter == 29) {
    const timer = setInterval(() => {
      price.innerHTML = counter;
      counter++;
      if (counter == 49) {
        setTimeout(() => {
          price.innerHTML = counter;
        }, 200);
        clearInterval(timer);
      }
    }, 20);
  } else {
    const timer = setInterval(() => {
      price.innerHTML = counter;
      counter--;
      if (counter == 29) {
        setTimeout(() => {
          price.innerHTML = counter;
        }, 200);
        clearInterval(timer);
      }
    }, 20);
  }
};

switchTerm.addEventListener("click", changePrice);


// >>>>>>>>>>>> Validate form <<<<<<<<<<<<<


const form = $(".card-body form");
const userName = $("#name");
const email = $("#email");
const password = $("#password");

const showErr = (input, mess) => {
  input.className = "form-input error";
  const formFloating = input.parentElement;
  const small = formFloating.querySelector("small");
  small.innerHTML = mess;
};

const showSussces = (input) => {
  input.className = "form-input";
  const formFloating = input.parentElement;
  const small = formFloating.querySelector("small");
  small.innerHTML = "";
};

const checkUserName = (input) => {
  const userName = input.value.trim();
  if (userName === "") {
    showErr(input, "Email is required!");
  } else {
    showSussces(input);
    return userName;
  }
};

const checkEmail = (input) => {
  const email = input.value.trim();
  const regex =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  if (regex.test(email)) {
    showSussces(input);
    return email;
  } else {
    showErr(input, "Email is not valid!");
  }
};

const checkPassword = (input) => {
  if (input.value.length < 8) {
    showErr(input, "Password must be at least 8 characters!");
  } else {
    showSussces(input);
    return input.value;
  }
};

form.addEventListener("submit", (e) => {
  e.preventDefault();

  const userNameValue = checkUserName(userName);
  const emailValue = checkEmail(email);
  const passwordValue = checkPassword(password);

  if (userNameValue && emailValue && passwordValue) {
    console.log({
      userName: userNameValue,
      email: emailValue,
      password: passwordValue,
    });
  }
});
